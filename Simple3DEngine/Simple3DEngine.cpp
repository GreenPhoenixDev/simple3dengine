#pragma once
#include "olcConsoleGameEngine.h"
#include <queue>
#include "Mesh.h"
#include "RenderingUtility.h"

using std::vector;
using std::sort;
using std::list;

class olcEngine3D : public olcConsoleGameEngine
{
public:
	olcEngine3D()
	{
		m_sAppName = L"3D Demo";
	}

	bool OnUserCreate() override
	{
		CubeMesh.LoadFromObjectFile("Shipwreck.obj");

		ProjectionMat = MakeProjectionMatrix(90.f, (float)ScreenHeight() / (float)ScreenWidth(), 0.1f, 1000.0f);

		return true;
	}
	bool OnUserUpdate(float ElapsedTime) override
	{
		// Clear Screen
		Fill(0, 0, ScreenWidth(), ScreenHeight(), PIXEL_SOLID, FG_BLACK);

		if (GetKey(VK_UP).bHeld)
			Camera.Y += 8.0f * ElapsedTime;

		if (GetKey(VK_DOWN).bHeld)
			Camera.Y -= 8.0f * ElapsedTime;

		if (GetKey(VK_RIGHT).bHeld)
			Camera.X -= 8.0f * ElapsedTime;

		if (GetKey(VK_LEFT).bHeld)
			Camera.X += 8.0f * ElapsedTime;

		Vector3D ForwardVec = MultiplyVectors(LookDir, 8.0f * ElapsedTime);

		if (GetKey(L'W').bHeld)
			Camera = AddVectors(Camera, ForwardVec);

		if (GetKey(L'S').bHeld)
			Camera = SubtractVectors(Camera, ForwardVec);

		if (GetKey(L'A').bHeld)
			Yaw -= 2.0f * ElapsedTime;

		if (GetKey(L'D').bHeld)
			Yaw += 2.0f * ElapsedTime;

		//fTheta += 1.0f * ElapsedTime;
		// Set up rotation matrices
		Matrix4x4 XRotMat, ZRotMat;
		XRotMat = MakeRotationMatrixX(Theta);
		ZRotMat = MakeRotationMatrixZ(Theta * 0.5f);

		Matrix4x4 TransMat;
		TransMat = MakeTranslationMatrix(0.0f, 0.0f, 5.0f);

		Matrix4x4 WorldMat;
		WorldMat = MakeIdentityMatrix();
		WorldMat = MultiplyMatrix(ZRotMat, XRotMat);
		WorldMat = MultiplyMatrix(WorldMat, TransMat);

		Vector3D UpVec{ 0.0f, 1.0f, 0.0f };
		Vector3D Target{ 0.0f, 0.0f, 1.0f };
		Matrix4x4 CameraRotMat = MakeRotationMatrixY(Yaw);
		LookDir = MatrixMultiplyVector(CameraRotMat, Target);
		Target = AddVectors(Camera, LookDir);

		Matrix4x4 CameraMat = PointAtMatrix(Camera, Target, UpVec);

		// Make view Matrix from camera
		Matrix4x4 ViewMat = QuickInverseMatrix(CameraMat);

		// Store triangles for rastering
		vector<Triangle>TrisToRaster;

		// Draw Triangles
		for (auto Tri : CubeMesh.Tris)
		{
			Triangle ProjectedTri, TransformedTri, ViewedTri;

			TransformedTri.P[0] = MatrixMultiplyVector(WorldMat, Tri.P[0]);
			TransformedTri.P[1] = MatrixMultiplyVector(WorldMat, Tri.P[1]);
			TransformedTri.P[2] = MatrixMultiplyVector(WorldMat, Tri.P[2]);

			// Calculate triangle normal
			Vector3D Normal, Line1, Line2;

			// Get the lines either side of the triangle
			Line1 = SubtractVectors(TransformedTri.P[1], TransformedTri.P[0]);
			Line2 = SubtractVectors(TransformedTri.P[2], TransformedTri.P[0]);

			// Take the cross product of lines to get the normal to the triangle surface
			Normal = CrossVectors(Line1, Line2);
			Normal = NormalizeVector(Normal);

			// Get Ray from triangle to camera
			Vector3D CameraRay = SubtractVectors(TransformedTri.P[0], Camera);

			if (DotVectors(Normal, CameraRay) < 0.0f)
			{
				// Illumination
				Vector3D LightDir{ 0.0f, 1.0f, -1.0f };
				LightDir = NormalizeVector(LightDir);

				// How aligned are the DirectionalLight and the triangle surface normal?
				float Dp = max(0.1f, DotVectors(LightDir, Normal));

				CHAR_INFO C = GetColour(Dp);
				TransformedTri.Col = C.Attributes;
				TransformedTri.Sym = C.Char.UnicodeChar;

				// Project triangles from World to View Space
				ViewedTri.P[0] = MatrixMultiplyVector(ViewMat, TransformedTri.P[0]);
				ViewedTri.P[1] = MatrixMultiplyVector(ViewMat, TransformedTri.P[1]);
				ViewedTri.P[2] = MatrixMultiplyVector(ViewMat, TransformedTri.P[2]);
				ViewedTri.Col = TransformedTri.Col;
				ViewedTri.Sym = TransformedTri.Sym;

				// Clip ViewedTri against NearClippingPlane, this could form two additional triangles
				int NClippedTris{ 0 };
				Triangle ClippedTris[2];
				NClippedTris = TriangleClipAgainstPlane({ 0.0f, 0.0f, 0.1f }, { 0.0f, 0.0f, 1.0f }, ViewedTri, ClippedTris[0], ClippedTris[1]);

				for (int N = 0; N < NClippedTris; N++)
				{
					// Project triangles from 3D to 2D Space
					ProjectedTri.P[0] = MatrixMultiplyVector(ProjectionMat, ClippedTris[N].P[0]);
					ProjectedTri.P[1] = MatrixMultiplyVector(ProjectionMat, ClippedTris[N].P[1]);
					ProjectedTri.P[2] = MatrixMultiplyVector(ProjectionMat, ClippedTris[N].P[2]);
					ProjectedTri.Col = ClippedTris[N].Col;
					ProjectedTri.Sym = ClippedTris[N].Sym;


					// Scale into view manually
					ProjectedTri.P[0] = DivideVectors(ProjectedTri.P[0], ProjectedTri.P[0].W);
					ProjectedTri.P[1] = DivideVectors(ProjectedTri.P[1], ProjectedTri.P[1].W);
					ProjectedTri.P[2] = DivideVectors(ProjectedTri.P[2], ProjectedTri.P[2].W);

					// X/Y are inverted so put them back
					ProjectedTri.P[0].X *= -1.0f;
					ProjectedTri.P[0].Y *= -1.0f;
					ProjectedTri.P[1].X *= -1.0f;
					ProjectedTri.P[1].Y *= -1.0f;
					ProjectedTri.P[2].X *= -1.0f;
					ProjectedTri.P[2].Y *= -1.0f;

					// Scale into view
					Vector3D OffsetView = { 1,1,0 };
					ProjectedTri.P[0] = AddVectors(ProjectedTri.P[0], OffsetView);
					ProjectedTri.P[1] = AddVectors(ProjectedTri.P[1], OffsetView);
					ProjectedTri.P[2] = AddVectors(ProjectedTri.P[2], OffsetView);
					ProjectedTri.P[0].X *= 0.5f * (float)ScreenWidth();
					ProjectedTri.P[0].Y *= 0.5f * (float)ScreenWidth();
					ProjectedTri.P[1].X *= 0.5f * (float)ScreenWidth();
					ProjectedTri.P[1].Y *= 0.5f * (float)ScreenWidth();
					ProjectedTri.P[2].X *= 0.5f * (float)ScreenWidth();
					ProjectedTri.P[2].Y *= 0.5f * (float)ScreenWidth();

					// Store triangle for rasterising
					TrisToRaster.push_back(ProjectedTri);
				}
			}
		}

		sort(TrisToRaster.begin(), TrisToRaster.end(), [](Triangle& T1, Triangle& T2)
			{
				float Z1 = (T1.P[0].Z + T1.P[1].Z + T1.P[2].Z) / 3.0f;
				float Z2 = (T2.P[0].Z + T2.P[1].Z + T2.P[2].Z) / 3.0f;
				return Z1 > Z2;
			});

		//Clear Screen
		Fill(0, 0, ScreenWidth(), ScreenHeight(), PIXEL_SOLID, FG_BLACK);

		for (auto& TriToRaster : TrisToRaster)
		{
			// Clip triangles against all four screen edges, this could yield a bunch of triangles
			Triangle Clipped[2];
			list<Triangle> ListTris;
			ListTris.push_back(TriToRaster);
			int NNewTris = 1;

			for (int P = 0; P < 4; P++)
			{
				int NTrisToAdd = 0;
				while (NNewTris > 0)
				{
					// Take triangle from front of queue
					Triangle Test = ListTris.front();
					ListTris.pop_front();
					NNewTris--;

					// Clip it against a plane. We only need to test each 
					// subsequent plane, against subsequent new triangles
					// as all triangles after a plane clip are guaranteed
					// to lie on the inside of the plane. I like how this
					// comment is almost completely and utterly justified
					switch (P)
					{
					case 0:
						NTrisToAdd = TriangleClipAgainstPlane({ 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, Test, Clipped[0], Clipped[1]);
						break;
					case 1:
						NTrisToAdd = TriangleClipAgainstPlane({ 0.0f, (float)ScreenHeight() - 1, 0.0f }, { 0.0f, -1.0f, 0.0f }, Test, Clipped[0], Clipped[1]);
						break;
					case 2:
						NTrisToAdd = TriangleClipAgainstPlane({ 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f }, Test, Clipped[0], Clipped[1]);
						break;
					case 3:
						NTrisToAdd = TriangleClipAgainstPlane({ (float)ScreenWidth() - 1, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f }, Test, Clipped[0], Clipped[1]);
						break;
					}

					// Clipping may yield a variable number of triangles, so
					// add these new ones to the back of the queue for subsequent
					// clipping against next planes
					for (int W = 0; W < NTrisToAdd; W++)
						ListTris.push_back(Clipped[W]);
				}
				NNewTris = ListTris.size();
			}


			// Draw the transformed, viewed, clipped, projected, sorted, clipped triangles
			for (auto& T : ListTris)
			{
				FillTriangle(T.P[0].X, T.P[0].Y, T.P[1].X, T.P[1].Y, T.P[2].X, T.P[2].Y, T.Sym, T.Col);
				//DrawTriangle(t.p[0].x, t.p[0].y, t.p[1].x, t.p[1].y, t.p[2].x, t.p[2].y, PIXEL_SOLID, FG_BLACK);
			}
		}

		return true;
	}

private:
	Mesh CubeMesh;
	Matrix4x4 ProjectionMat;

	Vector3D Camera;
	Vector3D LookDir;

	float Yaw{ 0.0f };
	float Theta{ 0.0f };
	// Taken From Command Line Webcam Video
	CHAR_INFO GetColour(float lum)
	{
		short bg_col, fg_col;
		wchar_t sym;
		int pixel_bw = (int)(13.0f * lum);
		switch (pixel_bw)
		{
		case 0: bg_col = BG_BLACK; fg_col = FG_BLACK; sym = PIXEL_SOLID; break;

		case 1: bg_col = BG_BLACK; fg_col = FG_DARK_GREY; sym = PIXEL_QUARTER; break;
		case 2: bg_col = BG_BLACK; fg_col = FG_DARK_GREY; sym = PIXEL_HALF; break;
		case 3: bg_col = BG_BLACK; fg_col = FG_DARK_GREY; sym = PIXEL_THREEQUARTERS; break;
		case 4: bg_col = BG_BLACK; fg_col = FG_DARK_GREY; sym = PIXEL_SOLID; break;

		case 5: bg_col = BG_DARK_GREY; fg_col = FG_GREY; sym = PIXEL_QUARTER; break;
		case 6: bg_col = BG_DARK_GREY; fg_col = FG_GREY; sym = PIXEL_HALF; break;
		case 7: bg_col = BG_DARK_GREY; fg_col = FG_GREY; sym = PIXEL_THREEQUARTERS; break;
		case 8: bg_col = BG_DARK_GREY; fg_col = FG_GREY; sym = PIXEL_SOLID; break;

		case 9:  bg_col = BG_GREY; fg_col = FG_WHITE; sym = PIXEL_QUARTER; break;
		case 10: bg_col = BG_GREY; fg_col = FG_WHITE; sym = PIXEL_HALF; break;
		case 11: bg_col = BG_GREY; fg_col = FG_WHITE; sym = PIXEL_THREEQUARTERS; break;
		case 12: bg_col = BG_GREY; fg_col = FG_WHITE; sym = PIXEL_SOLID; break;
		default:
			bg_col = BG_BLACK; fg_col = FG_BLACK; sym = PIXEL_SOLID;
		}

		CHAR_INFO c;
		c.Attributes = bg_col | fg_col;
		c.Char.UnicodeChar = sym;
		return c;
	}
};

int main()
{
	olcEngine3D Demo;
	if (Demo.ConstructConsole(550, 325, 4, 4))
		Demo.Start();

	return 0;
}