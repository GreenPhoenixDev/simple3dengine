#pragma once
#include "Vector3D.h"

struct Triangle
{
	Vector3D P[3];

	wchar_t Sym;
	short Col;

	bool operator<(const Triangle& Rhs) const
	{
		float Z1 = (this->P[0].Z + this->P[1].Z + this->P[2].Z) / 3.0f;
		float Z2 = (Rhs.P[0].Z + Rhs.P[1].Z + Rhs.P[2].Z) / 3.0f;
		return Z1 < Z2;
	}
};