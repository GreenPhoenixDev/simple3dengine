#pragma once
#include "Matrix.h"
#include "Vector3D.h"
#include <corecrt_math.h>

#define OUT

#pragma region Vector3D
Vector3D AddVectors(Vector3D& V1, Vector3D& V2)
{
	Vector3D V = { V1.X + V2.X, V1.Y + V2.Y, V1.Z + V2.Z };
	return V;
}
Vector3D SubtractVectors(Vector3D& V1, Vector3D& V2)
{
	Vector3D V = { V1.X - V2.X, V1.Y - V2.Y, V1.Z - V2.Z };
	return V;
}
Vector3D MultiplyVectors(Vector3D& V, float F)
{
	Vector3D V1 = { V.X * F, V.Y * F, V.Z * F };
	return V1;
}
Vector3D DivideVectors(Vector3D& V, float F)
{
	Vector3D V1 = { V.X / F, V.Y / F, V.Z / F };
	return V1;
}
float DotVectors(Vector3D& V1, Vector3D& V2)
{
	return V1.X * V2.X + V1.Y * V2.Y + V1.Z * V2.Z;
}
float VectorLength(Vector3D& V)
{
	return sqrtf(DotVectors(V, V));
}
Vector3D NormalizeVector(Vector3D& V)
{
	float L = VectorLength(V);
	Vector3D V1 = { V.X / L, V.Y / L, V.Z / L };
	return V1;
}
Vector3D CrossVectors(Vector3D& V1, Vector3D& V2)
{
	Vector3D V;
	V.X = V1.Y * V2.Z - V1.Z * V2.Y;
	V.Y = V1.Z * V2.X - V1.X * V2.Z;
	V.Z = V1.X * V2.Y - V1.Y * V2.X;
	return V;
}
Vector3D MatrixMultiplyVector(Matrix4x4& M, Vector3D& V)
{
	Vector3D Ov;
	Ov.X = V.X * M.M[0][0] + V.Y * M.M[1][0] + V.Z * M.M[2][0] + V.W * M.M[3][0];
	Ov.Y = V.X * M.M[0][1] + V.Y * M.M[1][1] + V.Z * M.M[2][1] + V.W * M.M[3][1];
	Ov.Z = V.X * M.M[0][2] + V.Y * M.M[1][2] + V.Z * M.M[2][2] + V.W * M.M[3][2];
	Ov.W = V.X * M.M[0][3] + V.Y * M.M[1][3] + V.Z * M.M[2][3] + V.W * M.M[3][3];
	return Ov;
}
#pragma endregion
#pragma region Matrix4x4
Matrix4x4 MakeIdentityMatrix()
{
	Matrix4x4 Matrix;
	Matrix.M[0][0] = 1.0f;
	Matrix.M[1][1] = 1.0f;
	Matrix.M[2][2] = 1.0f;
	Matrix.M[3][3] = 1.0f;
	return Matrix;
}
Matrix4x4 MakeRotationMatrixX(float AngleRad)
{
	Matrix4x4 Matrix;
	Matrix.M[0][0] = 1.0f;
	Matrix.M[1][1] = cosf(AngleRad);
	Matrix.M[1][2] = sinf(AngleRad);
	Matrix.M[2][1] = -sinf(AngleRad);
	Matrix.M[2][2] = cosf(AngleRad);
	Matrix.M[3][3] = 1.0f;
	return Matrix;
}
Matrix4x4 MakeRotationMatrixY(float AngleRad)
{
	Matrix4x4 Matrix;
	Matrix.M[0][0] = cosf(AngleRad);
	Matrix.M[0][2] = sinf(AngleRad);
	Matrix.M[2][0] = -sinf(AngleRad);
	Matrix.M[1][1] = 1.0f;
	Matrix.M[2][2] = cosf(AngleRad);
	Matrix.M[3][3] = 1.0f;
	return Matrix;
}
Matrix4x4 MakeRotationMatrixZ(float AngleRad)
{
	Matrix4x4 Matrix;
	Matrix.M[0][0] = cosf(AngleRad);
	Matrix.M[0][1] = sinf(AngleRad);
	Matrix.M[1][0] = -sinf(AngleRad);
	Matrix.M[1][1] = cosf(AngleRad);
	Matrix.M[2][2] = 1.0f;
	Matrix.M[3][3] = 1.0f;
	return Matrix;
}
Matrix4x4 MakeTranslationMatrix(float X, float Y, float Z)
{
	Matrix4x4 Matrix;
	Matrix.M[0][0] = 1.0f;
	Matrix.M[1][1] = 1.0f;
	Matrix.M[2][2] = 1.0f;
	Matrix.M[3][3] = 1.0f;
	Matrix.M[3][0] = X;
	Matrix.M[3][1] = Y;
	Matrix.M[3][2] = Z;
	return Matrix;
}
Matrix4x4 MakeProjectionMatrix(float FovDegrees, float AspectRatio, float Near, float Far)
{
	float FovRad = 1.0f / tanf(FovDegrees * 0.5f / 180.0f * 3.14159f);
	Matrix4x4 Matrix;
	Matrix.M[0][0] = AspectRatio * FovRad;
	Matrix.M[1][1] = FovRad;
	Matrix.M[2][2] = Far / (Far - Near);
	Matrix.M[3][2] = (-Far * Near) / (Far - Near);
	Matrix.M[2][3] = 1.0f;
	Matrix.M[3][3] = 0.0f;
	return Matrix;
}
Matrix4x4 MultiplyMatrix(Matrix4x4& M1, Matrix4x4& M2)
{
	Matrix4x4 Matrix;
	for (int C = 0; C < 4; C++)
	{
		for (int R = 0; R < 4; R++)
		{
			Matrix.M[R][C] = M1.M[R][0] * M2.M[0][C] + M1.M[R][1] * M2.M[1][C] + M1.M[R][2] * M2.M[2][C] + M1.M[R][3] * M2.M[3][C];
		}
	}
	return Matrix;
}
Matrix4x4 PointAtMatrix(Vector3D& Pos, Vector3D& Target, Vector3D& Up)
{
	// Calculate new forward direction
	Vector3D NewForward = SubtractVectors(Target, Pos);
	NewForward = NormalizeVector(NewForward);

	// Calculate new Up direction
	Vector3D A = MultiplyVectors(NewForward, DotVectors(Up, NewForward));
	Vector3D NewUp = SubtractVectors(Up, A);
	NewUp = NormalizeVector(NewUp);

	// New Right direction is easy, its just cross product
	Vector3D NewRight = CrossVectors(NewUp, NewForward);

	// Construct Dimensioning and Translation Matrix	
	Matrix4x4 Matrix;
	Matrix.M[0][0] = NewRight.X;	
	Matrix.M[0][1] = NewRight.Y;	
	Matrix.M[0][2] = NewRight.Z;	
	Matrix.M[0][3] = 0.0f;

	Matrix.M[1][0] = NewUp.X;		
	Matrix.M[1][1] = NewUp.Y;		
	Matrix.M[1][2] = NewUp.Z;		
	Matrix.M[1][3] = 0.0f;

	Matrix.M[2][0] = NewForward.X;	
	Matrix.M[2][1] = NewForward.Y;	
	Matrix.M[2][2] = NewForward.Z;	
	Matrix.M[2][3] = 0.0f;

	Matrix.M[3][0] = Pos.X;			
	Matrix.M[3][1] = Pos.Y;			
	Matrix.M[3][2] = Pos.Z;			
	Matrix.M[3][3] = 1.0f;
	return Matrix;
}
Matrix4x4 QuickInverseMatrix(Matrix4x4& M) // Only for Rotation/Translation Matrices
{
	Matrix4x4 Matrix;
	Matrix.M[0][0] = M.M[0][0];
	Matrix.M[0][1] = M.M[1][0];
	Matrix.M[0][2] = M.M[2][0];
	Matrix.M[0][3] = 0.0f;

	Matrix.M[1][0] = M.M[0][1];
	Matrix.M[1][1] = M.M[1][1];
	Matrix.M[1][2] = M.M[2][1];
	Matrix.M[1][3] = 0.0f;

	Matrix.M[2][0] = M.M[0][2];
	Matrix.M[2][1] = M.M[1][2];
	Matrix.M[2][2] = M.M[2][2];
	Matrix.M[2][3] = 0.0f;

	Matrix.M[3][0] = -(M.M[3][0] * Matrix.M[0][0] + M.M[3][1] * Matrix.M[1][0] + M.M[3][2] * Matrix.M[2][0]);
	Matrix.M[3][1] = -(M.M[3][0] * Matrix.M[0][1] + M.M[3][1] * Matrix.M[1][1] + M.M[3][2] * Matrix.M[2][1]);
	Matrix.M[3][2] = -(M.M[3][0] * Matrix.M[0][2] + M.M[3][1] * Matrix.M[1][2] + M.M[3][2] * Matrix.M[2][2]);
	Matrix.M[3][3] = 1.0f;
	return Matrix;
}
#pragma endregion
#pragma region Clipping
Vector3D VectorIntersectPlane(Vector3D& PlaneP, Vector3D& PlaneN, Vector3D& LineStart, Vector3D& LineEnd)
{
	PlaneN = NormalizeVector(PlaneN);
	float PlaneD = -DotVectors(PlaneN, PlaneP);
	float Ad = DotVectors(LineStart, PlaneN);
	float Bd = DotVectors(LineEnd, PlaneN);
	float T = (-PlaneD - Ad) / (Bd - Ad);
	Vector3D LineStartToEnd = SubtractVectors(LineEnd, LineStart);
	Vector3D LineToIntersect = MultiplyVectors(LineStartToEnd, T);
	return AddVectors(LineStart, LineToIntersect);
}
int TriangleClipAgainstPlane(Vector3D PlaneP, Vector3D PlaneN, Triangle& InTri, Triangle& OUT Tri1, Triangle& OUT Tri2)
{
	// Make sure plane normal is indeed normal
	PlaneN = NormalizeVector(PlaneN);

	// Return signed shortest distance from point to plane, plane normal must be normalised
	auto Dist = [&](Vector3D& P)
	{
		Vector3D N = NormalizeVector(P);
		return (PlaneN.X * P.X + PlaneN.Y * P.Y + PlaneN.Z * P.Z - DotVectors(PlaneN, PlaneP));
	};

	// Create two temporary storage arrays to classify points either side of plane
	// If distance sign is positive, point lies on "inside" of plane
	Vector3D* InsidePoints[3];  
	int NInsidePointCount = 0;
	Vector3D* OutsidePoints[3];
	int NOutsidePointCount = 0;

	// Get signed distance of each point in triangle to plane
	float D0 = Dist(InTri.P[0]);
	float D1 = Dist(InTri.P[1]);
	float D2 = Dist(InTri.P[2]);

	if (D0 >= 0)
		InsidePoints[NInsidePointCount++] = &InTri.P[0];
	else 
		OutsidePoints[NOutsidePointCount++] = &InTri.P[0];

	if (D1 >= 0)
		InsidePoints[NInsidePointCount++] = &InTri.P[1];
	else 
		OutsidePoints[NOutsidePointCount++] = &InTri.P[1];

	if (D2 >= 0)
		InsidePoints[NInsidePointCount++] = &InTri.P[2];
	else 
		OutsidePoints[NOutsidePointCount++] = &InTri.P[2];

	// Now classify triangle points, and break the input triangle into 
	// smaller output triangles if required. There are four possible
	// outcomes...

	if (NInsidePointCount == 0)
	{
		// All points lie on the outside of plane, so clip whole triangle
		// It ceases to exist

		return 0; // No returned triangles are valid
	}

	if (NInsidePointCount == 3)
	{
		// All points lie on the inside of plane, so do nothing
		// and allow the triangle to simply pass through
		Tri1 = InTri;

		return 1; // Just the one returned original triangle is valid
	}

	if (NInsidePointCount == 1 && NOutsidePointCount == 2)
	{
		// Triangle should be clipped. As two points lie outside
		// the plane, the triangle simply becomes a smaller triangle

		// Copy appearance info to new triangle
		Tri1.Col = InTri.Col;
		Tri1.Sym = InTri.Sym;

		// The inside point is valid, so keep that...
		Tri1.P[0] = *InsidePoints[0];

		// but the two new points are at the locations where the 
		// original sides of the triangle (lines) intersect with the plane
		Tri1.P[1] = VectorIntersectPlane(PlaneP, PlaneN, *InsidePoints[0], *OutsidePoints[0]);
		Tri1.P[2] = VectorIntersectPlane(PlaneP, PlaneN, *InsidePoints[0], *OutsidePoints[1]);

		return 1; // Return the newly formed single triangle
	}

	if (NInsidePointCount == 2 && NOutsidePointCount == 1)
	{
		// Triangle should be clipped. As two points lie inside the plane,
		// the clipped triangle becomes a "quad". Fortunately, we can
		// represent a quad with two new triangles

		// Copy appearance info to new triangles
		Tri1.Col = InTri.Col;
		Tri1.Sym = InTri.Sym;

		Tri2.Col = InTri.Col;
		Tri2.Sym = InTri.Sym;

		// The first triangle consists of the two inside points and a new
		// point determined by the location where one side of the triangle
		// intersects with the plane
		Tri1.P[0] = *InsidePoints[0];
		Tri1.P[1] = *InsidePoints[1];
		Tri1.P[2] = VectorIntersectPlane(PlaneP, PlaneN, *InsidePoints[0], *OutsidePoints[0]);

		// The second triangle is composed of one of he inside points, a
		// new point determined by the intersection of the other side of the 
		// triangle and the plane, and the newly created point above
		Tri2.P[0] = *InsidePoints[1];
		Tri2.P[1] = Tri1.P[2];
		Tri2.P[2] = VectorIntersectPlane(PlaneP, PlaneN, *InsidePoints[1], *OutsidePoints[0]);

		return 2; // Return two newly formed triangles which form a quad
	}
}
#pragma endregion