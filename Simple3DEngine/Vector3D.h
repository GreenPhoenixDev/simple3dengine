#pragma once

struct Vector3D
{
public:
	float X{ 0 };
	float Y{ 0 };
	float Z{ 0 };
	float W{ 1 };
};